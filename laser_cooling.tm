<TeXmacs|2.1>

<style|<tuple|generic|british>>

<\body>
  <doc-data|<doc-title|Laser cooling and trapping>|<doc-author|<author-data|<author-name|<strong|Franck
  Pereira Dos Santos>>>>>

  <section|Magnetic traps>

  Atoms or paramagnetic molecules do interact with <strong|gradients of
  static magnetic field>. An atom in a Zeeman state feels a force:

  <\equation>
    F\<sim\>-\<nabla\>E<around*|(|B|)>
  </equation>

  This make the Stern Gerlach effect.

  <\theorem>
    <dueto|Wing> The modulus of a magnetic or electric field cannot have a
    maxium in a region free of charges and currents
  </theorem>

  Typically, atms are tranfered from a MOT or a molasses in the magnetic
  trap.

  <\itemize>
    <item>Optical pumping in the \Plow field seeking\Q states increase
    loading efficiency

    <item>Mode matched transfer in the quadrupole moment
  </itemize>

  <section|Dipole traps>

  Use of far off resonance lasers.\ 

  <section|Evaporative cooling>

  Some atoms leave the trap with part of the energy. For a loss of a factor
  10 in <math|N> you'll loose <math|10<rsup|\<alpha\>>> with
  <math|\<alpha\>\<gtr\>1>

  <\itemize>
    <item>Runaway regime of evaporation

    <item>Eventually reach quantum degeneracy
  </itemize>

  Optimization of the evaporation: kinetic aspects.

  Choice of <math|U<rsub|t>> (trap depth) high for efficient cooling but the
  rate of good events is low and the temperature decreases.

  <section|Bose Einstein condensation>

  At very low temperature bosons can occupy the lowest quantum state. They
  form a collective quantum wave.

  <section|Miscellaneous>

  <subsection|Delta kick collimation>

  You turn off the trap, let atoms expand, then the momentum of the atom is
  linear with position. Then a kick changed the momentum of the toms by
  <math|\<Delta\>p\<sim\>\<nabla\>U\<sim\>x\<sim\>p>. Thus atom is at rest
  after the pulse.

  N.b: when you turn off the trap, atoms start falling ! Experience of this
  type are made in free fall (microgravity).

  <subsection|Cold atom gravimeter>

  Allow to measure non inertial effect in a satellite, used to do measurement
  of the field. With electrostatic interferometer.

  \;
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../../../.TeXmacs/texts/scratch/no_name_4.tm>>
    <associate|auto-2|<tuple|2|1|../../../.TeXmacs/texts/scratch/no_name_4.tm>>
    <associate|auto-3|<tuple|3|1|../../../.TeXmacs/texts/scratch/no_name_4.tm>>
    <associate|auto-4|<tuple|4|?|../../../.TeXmacs/texts/scratch/no_name_4.tm>>
    <associate|auto-5|<tuple|5|?|../../../.TeXmacs/texts/scratch/no_name_4.tm>>
    <associate|auto-6|<tuple|5.1|?|../../../.TeXmacs/texts/scratch/no_name_4.tm>>
    <associate|auto-7|<tuple|5.2|?|../../../.TeXmacs/texts/scratch/no_name_4.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Magnetic
      traps> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Dipole
      traps> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>Evaporative
      cooling> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>